from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

AUTHOR_TODAY_HOMEPAGE_URL = 'https://author.today/'


class AuthorTodayHomepageTests(unittest.TestCase):
    def setUp(self) -> None:
        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        self.driver = webdriver.Remote(command_executor='http://selenium__standalone-chrome:4444/wd/hub',
                                       options=options)
        self.driver.get(AUTHOR_TODAY_HOMEPAGE_URL)

    def tearDown(self) -> None:
        self.driver.quit()

    def test_login_popup_displayed(self) -> None:
        self.driver.implicitly_wait(5)
        login_button = self.driver.find_element(By.XPATH, '//*[@id="navbar-right"]/li[2]')
        login_button.click()
        login_popup = WebDriverWait(self.driver, 3).until(EC.visibility_of_element_located(
            (By.XPATH, '//*[@id="authModal"]/div')))
        self.assertTrue(login_popup.is_displayed())

    def test_homepage_navigation_bar_titles(self) -> None:
        self.driver.implicitly_wait(5)
        actual_nav_bar = self.driver.find_elements(By.XPATH, '//*[@id="navbar"]/ul/li')
        actual_nav_items = set(
            map(lambda item: item.text, actual_nav_bar)
        )
        expected_nav_items = {'', 'Моя библиотека', 'Книги', 'Сообщество', 'Обсуждения'}
        self.assertSetEqual(actual_nav_items, expected_nav_items)

    def test_support_email(self) -> None:
        self.driver.implicitly_wait(5)
        support_email = self.driver.find_element(By.XPATH, '/html/body/footer/div/div/div[3]/div[2]/a')
        self.assertEqual(support_email.text, 'support@author.today')


if __name__ == '__main__':
    unittest.main()
