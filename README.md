# Lab 8 - Exploratory testing and UI

Website used for testing: [Author.today](https://www.author.today)

## Test 1
Steps:
1. Go to [https://www.author.today](https://www.author.today)
2. Find login button using *XPath*
3. Click it
4. Wait until window is displayed (no more than 3 seconds)
5. Ensure it is shown on the screen

## Test 2
Steps:
1. Go to [https://www.author.today](https://www.author.today)
2. Use *XPath* to get navigation bar elements.
3. Ensure that navigation bar element titles are present successfully.

## Test 3
Steps:
1. Go to [https://www.author.today](https://www.author.today)
2. Use *XPath* to get support email
3. Ensure email is correct